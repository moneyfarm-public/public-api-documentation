# Intro

Brief introduction to the Moneyfarm Public API.

## Documentation
[Authentication and Authorisation](docs/authorisation-authentication.md)

[JWT and JWKS usage and requirements](docs/jwt-jwks.md)

## OpenAPI Specification (OAS) 
Here you can find the detailed specification of our Public API in openapi3 standard.
[OpenAPI Specification](../openapi/v0/openapi.yaml)

Detailed specification of the next version of our Public API in openapi3 standard. 

**Note that this is still not fully stable and should not be used yet.**


[OpenAPI Specification v1](../openapi/v1/openapi.yaml)