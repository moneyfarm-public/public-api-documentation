# Context

In the Public API, all Authentication and Authorisation concerns are based on JWT tokens.

Access to any endpoint is only available through the presence of a valid JWT as a bearer token issued and signed by an authority recognised by the Moneyfarm platform.

More details about the JWT and JWKS usage on our API can be found here: [JWT and JWKS usage and requirements](jwt-jwks.md)

## Authentication

*Determines whether users are who they claim to be*
Authentication of the requesting entity is done by validation of the signature of the JWT against the keys we hold for our partners in the form of JWKS. For example, all Auth0 tenants will this have available at `https://YOUR_DOMAIN/.well-known/jwks.json`

By default, each partner should provide a single key by environment available on a well-known endpoint that must be reachable by our platform at all times. If this information is not available all attempts to use the `/login/token` endpoint will fail.


Authentication of the user making the requests is done when the Moneyfarm JWT is issued via the `/login/token` endpoint and relies on the validation that the supplied **external-id** and **partner** is a valid combination in our records.

*We delegate user authentication to our partners and trust that the user identified on the JWT used for login is correct.*

## Authorisation

*Determines what users can and cannot access*

For public routes all access is protected through Resource-Based Authorisation, meaning that the requester can only obtain, and manage, information for resources they own.

  

## Open Routes

*Collection of all operations that do not target a specific user.*

These include the operations that allow our partners to obtain information about our product offering, simulate performance and issue recommendations for their customers on which product is best suited for them before onboarding them to our platform.

All these operations do not require that the JWT be issued by Moneyfarm. The *{PartnerJWT}* identified on the diagram below is issued the trusted partner and does not need to include the *external-id* claim. 

All information retrievable by these operations is related to the Partner, and not to a particular user. 

Note that these operations can be accessed with a *{MFM_JWT}* that was issued by a previous login, and the behaviour will be the same as when the *{PartnerJWT}* is used.

  

![Alt text](../images/open-routes.jpg "stuff")

  

A special case of these operations is the `/login/token` that although not requiring a Moneyfarm issued JWT, requires the partner JWT to identify the customer that is logging in.

  

## Public Routes

*Collection of all operations that target specific user information*

The bulk of our Public API offering falls under this category with access only granted through a JWT issued by Moneyfarm through the `/login/token` operation.

  
![Alt text](../images/public-routes.jpg "stuff")

As described in the diagram above, to access the operations available on Public-Routes, there is a requirement to obtain a *{MFM_JWT}* through the **login** operation using the *{PartnerJWT}* containing the *external-id* claim identifying the customer. 

The *{MFM_JWT}* contains the required authorisation information to be used with subsequent calls to the Public API. With exception of the expiry, this should be treated as a black box by our partners. 

If required, the public JWKS can be used to validate that the token was issued by Moneyfarm for that environment by checking the JWT signature.