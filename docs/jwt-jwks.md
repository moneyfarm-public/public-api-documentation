# JWT
  JSON Web Tokens are an open, industry standard [RFC 7519](https://tools.ietf.org/html/rfc7519) method for representing claims securely between two parties. More information and tools can be found at [JWT.io](https://jwt.io/)


**Example of an encoded JWT issued by Moneyfarm:**
```
eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6Ik4wUXhOVGxGTmpSRk5FWkRNRGcyTkRCQ016VXdOVEZDUmpVd1JEWkNSVFU0TVRkRU1UUkNSUSJ9.eyJodHRwczovL21vbmV5ZmFybS5jb20vYmFzaWMiOnsidXNlcklkIjo1MzY1MCwicGxhdGZvcm0iOiJHQiIsInBhcnRuZXIiOiJNT05FWUZBUk0iLCJyb2xlcyI6W3siYXV0aG9yaXR5IjoiUk9MRV9VU0VSIiwiaWQiOjJ9XSwidGhpcmRwYXJ0eSI6Im1vbmV5ZmFybSJ9LCJpc3MiOiJodHRwczovL21mLXRlc3QuZXUuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDUzNjUwIiwiYXVkIjoiaHR0cHM6Ly9hdXRoLXRlc3QubW9uZXlmYXJtLmNvbS91c2VyaW5mbyIsImlhdCI6MTYwMDc2MjgzMywiZXhwIjoxNjAwODQ5MjMzLCJhenAiOiJkN3luWWhUOTYycWRFRjJETnNZMEJIQU9Wdk53bVlobSIsImd0eSI6InBhc3N3b3JkIn0.A1m7mBc0HBo5Kb98EHo91zVeOa1G-m7YLYaWJT4njfE9gLQoFpK2yk9qJnAYDKRmZYDEFMfRCPWHArrPJMsbUHUJM9S6T63xanVA-nKZLwJMIafPSLMDEyCG-_VLCfKxoSmfQWaImOfWijDJHVKsZM_kx6grt3fDTOinJmy0juX-xb7qJLsXYmiMEpDiYesEVGytLJT90xQJbKLm2foWEvlkIgfnW9gkhhRjAKaBIQd0vSktN0cSP4M-iHQ__3nNRvpmiINhpaJin8qj2Qk9ZjhVxPUBjtERJcPAcIxt_nQ3iazrYCeeA3Q_7-QsW1oebTP7mdABlHlAxSw6Y746AA
```

**Decoded JWT header:**
```json
{
  "alg": "RS256",
  "typ": "JWT",
  "kid": "N0QxNTlFNjRFNEZDMDg2NDBCMzUwNTFCRjUwRDZCRTU4MTdEMTRCRQ"
}
```
**Decoded JWT payload:**
```json
{
  "https://moneyfarm.com/basic": {
    "userId": 53650,
    "platform": "GB",
    "partner": "MONEYFARM",
    "roles": [
      {
        "authority": "ROLE_USER",
        "id": 2
      }
    ],
    "thirdparty": "moneyfarm"
  },
  "iss": "https://mf-test.eu.auth0.com/",
  "sub": "auth0|53650",
  "aud": "https://api-test.moneyfarm.com/",
  "iat": 1600762833,
  "exp": 1600849233,
  "azp": "d7ynYhT962qdEF2DNsY0BHAOVvNwmYhm",
  "gty": "password"
}
```


## Partner JWT
For our partners to access the Public API we require that they provide an example JWT issued by them, or on their behalf by a trusted source, and access to the public key in JWKS format for us to validate the token signature. 
This token must conform to the rules described above. 

After the token validation is performed we will add it to our trusted partnership list and onboard the partner configuration.
Once this process is finished the partner can start using our Open Routes as described below.
**note: Tokens are not transferable across environments. We expect that tokens from the same partner to have different signatures and issuers for each environment. **


### Expected fields
Besides the mandatory claims described on RFC 7519, the following are also required for use with our Public API:

 1. "iss" (Issuer) Claim: Used to identify who issued the JWT. 
    1. This needs to be present at the root level of the JWT body.
    2. Have distinct values for each environment. For example tokens for our _Test_ environment have `"iss": "https://mf-test.eu.auth0.com/"` and for _Production_ have `"iss": "https://auth.moneyfarm.com/"`
 2. "exp" (Expiration Time) Claim: Used to validate that the token has not yet expired. Expired tokens, or tokens without this claim, will not be acceptable.
    1. This needs to be present at the root level of the JWT body.
 3. __External Customer Id__ Claim: Identifying the customer on the partners systems and used for mapping to our internal identifier.
    1. This must be the same identifier that is provided during the customers onboarding.
    2. By default the field to use is the `"sub"` claim.
    2. If required a customised claim can be used but requires a customisation on our side.
    3. *This is only required for the login request and the final name of the claim can be configured at a Partner level*


# JWKS
The JSON Web Key Set (JWKS), as described on [RFC517](https://tools.ietf.org/html/rfc7517), is a set of keys containing the public keys used to verify any JSON Web Token (JWT) issued by the authorization server.

Since all access to Moneyfarms Public API is authorised via JWT we require access to the JWKS of the partners issuers. This is how our API Gateway obtains the public keys to validate the token signature and also identify the partner that is calling our API.


**Example of JWKS**
```
{
	"keys": [
		{
			"alg": "RS256",
			"kty": "RSA",
			"use": "sig",
			"n": "zEpHtH7rShU1Pgkd0QB4ifXJF5OcKbvvhxgmF6FQdQ1W_zAHIzvLSmGZhXIxtBbcqaEfNwbRotWmUq5Hly5QfOAUwmqpwaFraWlB1_UsNCLbtIfCAhjJpDWfmxPiAn5xNNl7c6nzbCNuo08vLMrctPOFO9fa1ahckF8ZcYCyd0JHuLA4x0AS4mzP0vMY2CSzsJ_ttEfH_HdqMNzrHISps8bOh53XkBBJAv26K8H60YzBW1KjzBp-GHGXn2YGYQhUpgjCLXGajzTIiGwzT-I8a2zGlUXtROYkETCSLDXb6cIwjpYNs9h6JCknbR_BnIjHxgiW58vTFHSl4SgpS5MjCQ",
			"e": "AQAB",
			"kid": "N0QxNTlFNjRFNEZDMDg2NDBCMzUwNTFCRjUwRDZCRTU4MTdEMTRCRQ",
			"x5t": "N0QxNTlFNjRFNEZDMDg2NDBCMzUwNTFCRjUwRDZCRTU4MTdEMTRCRQ",
			"x5c": [
				"MIIDAzCCAeugAwIBAgIJN8oti26m43h7MA0GCSqGSIb3DQEBCwUAMB8xHTAbBgNVBAMTFG1mLXRlc3QuZXUuYXV0aDAuY29tMB4XDTE4MDMxMzE3MTM1N1oXDTMxMTEyMDE3MTM1N1owHzEdMBsGA1UEAxMUbWYtdGVzdC5ldS5hdXRoMC5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDMSke0futKFTU+CR3RAHiJ9ckXk5wpu++HGCYXoVB1DVb/MAcjO8tKYZmFcjG0FtypoR83BtGi1aZSrkeXLlB84BTCaqnBoWtpaUHX9Sw0Itu0h8ICGMmkNZ+bE+ICfnE02XtzqfNsI26jTy8syty084U719rVqFyQXxlxgLJ3Qke4sDjHQBLibM/S8xjYJLOwn+20R8f8d2ow3OschKmzxs6HndeQEEkC/borwfrRjMFbUqPMGn4YcZefZgZhCFSmCMItcZqPNMiIbDNP4jxrbMaVRe1E5iQRMJIsNdvpwjCOlg2z2HokKSdtH8GciMfGCJbny9MUdKXhKClLkyMJAgMBAAGjQjBAMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFGo7QEI7VajdrcEDDOyA2f0OY1ypMA4GA1UdDwEB/wQEAwIChDANBgkqhkiG9w0BAQsFAAOCAQEAcSXAXa/qPVZCVHKLfz2wO01vbEkClY1KSozv58ac6yHNM2pIt59jDOHxfsFBGgMhsiirAWdlJQVsdcaFX8dhRrDmHQf9LithJLKyC8xZ6MjVnDM/5WYGWtgVWAqNcH1+/x7xfrUgDAV7CjXRV+A8/ncD2ffilcopBA9RIMxSaTxSgeaJTBjshYQvHoDxlXRzcOTPfUVR0ca2JWD4C+Xfr9qirfSxLAnKe9zPfkf4uORshYpiuo9+XRVsUmMZpV4YfzriEYFA4xOJ2CI0BK0NhQlxjTQy7QmZlso7hFZT/bsUPCMOuWJCvtZnm+QvN/xB4wGztuoKDU4q2bU/mIucvQ=="
			]
		}
	]
}
```
## Moneyfarm JWKS
If a partner would like to validate the authenticity of the JWTs issued by Moneyfarm, we provide publicly available JWKS for such purposes. These keys can be rotated over time to maintain a high level of confidence in their integratty.

Moneyfarm's public key sets can be found here :
- Test environment: https://auth-test.moneyfarm.com/.well-known/jwks.json
- Production environment: https://auth.moneyfarm.com/.well-known/jwks.json

## Partner JWKS
For our partners to use the Moneyfarm Public API they are required to provide access to a JWKS containing the public keys that can be used to verify their JWTs signature.

It is the responsability of the partners to inform Moneyfarm, with as much advance as possible, of changes to the keys being used to sign their JWTs. Failure to do so might result in temporary lack of access to our API. 

_We do encourage that these JWKS are publicly available, and reachable without authentication requirements._

### Expected fields
Besides the mandatory claims described on [RFC517](https://tools.ietf.org/html/rfc7517), the following are also required for use with our Public API:
1. `alg`:	The specific cryptographic algorithm used with the key.
2. `use`:	How the key was meant to be used; `"sig"` represents the signature.
3. `kid`:	The unique identifier for the key.
4. `n`:	The modulus for the RSA public key.
5. `e`:	The exponent for the RSA public key.
